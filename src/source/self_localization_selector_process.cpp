/*!*******************************************************************************************
 *  \file       self_localization_selector_process.cpp
 *  \brief      SelfLocalizationSelector implementation file.
 *  \details    This file implements the SelfLocalizationSelector class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/self_localization_selector_process.h"

SelfLocalizationSelector::SelfLocalizationSelector()
{
  mode = odometry;
}

SelfLocalizationSelector::~SelfLocalizationSelector()
{
}

/*DroneProcess*/
void SelfLocalizationSelector::ownSetUp()
{
  ros::NodeHandle private_handle("~");

  private_handle.param<std::string>("drone_id", drone_id, "1");
  private_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  private_handle.param<std::string>("my_stack_directory", my_stack_directory,
                                    "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  private_handle.param<std::string>("change_mode_to_odometry_srv", change_mode_to_odometry_str,
                                    "change_self_localization_mode_to_odometry");

  private_handle.param<std::string>("change_mode_to_ekf_sensor_fusion", change_mode_to_ekf_sensor_fusion_str,
                                    "change_self_localization_mode_to_ekf_sensor_fusion");
  
  private_handle.param<std::string>("change_mode_to_visual_markers_srv", change_mode_to_visual_markers_str,
                                    "change_self_localization_mode_to_visual_markers");
  private_handle.param<std::string>("change_mode_to_frame_based_localization_srv",
                                    change_mode_to_frame_based_localization_str,
                                    "change_self_localization_mode_to_frame_based_localization");
  private_handle.param<std::string>("odometry_estimated_pose_topic", odometry_estimated_pose_str,
                                    "EstimatedPose_droneGMR_wrt_GFF");
  private_handle.param<std::string>("odometry_estimated_speed_topic", odometry_estimated_speed_str,
                                    "EstimatedSpeed_droneGMR_wrt_GFF");
  private_handle.param<std::string>("aruco_slam_estimated_pose_topic", aruco_slam_estimated_pose_str,
                                    "ArucoSlam_EstimatedPose");
  private_handle.param<std::string>("aruco_slam_estimated_speed_topic", aruco_slam_estimated_speed_str,
                                    "ArucoSlam_EstimatedSpeed");
  private_handle.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
  private_handle.param<std::string>("estimated_speed_topic", estimated_speed_str, "estimated_speed");
  private_handle.param<std::string>("pose_topic", geometric_pose_str, "self_localization/pose");
  private_handle.param<std::string>("speed_topic", geometric_speed_str, "self_localization/speed");
  private_handle.param<std::string>("frame_based_estimated_pose_topic", frame_based_estimated_pose_str,
                                    "frame_localizer_estimated_pose");
  private_handle.param<std::string>("frame_based_estimated_speed_topic", frame_based_estimated_speed_str,
                                    "EstimatedSpeed_droneGMR_wrt_GFF");

  change_mode_to_odometry_srv = node_handle.advertiseService(
      change_mode_to_odometry_str, &SelfLocalizationSelector::changeModeToOdometryCallback, this);
  change_mode_to_ekf_sensor_fusion_srv = node_handle.advertiseService(
      change_mode_to_ekf_sensor_fusion_str, &SelfLocalizationSelector::changeModeToEkfSensorFusionCallback, this);
  change_mode_to_visual_markers_srv = node_handle.advertiseService(
      change_mode_to_visual_markers_str, &SelfLocalizationSelector::changeModeToVisualMarkersCallback, this);
  change_mode_to_frame_based_localization_srv =
      node_handle.advertiseService(change_mode_to_frame_based_localization_str,
                                   &SelfLocalizationSelector::changeModeToFrameBasedLocalizationCallback, this);
}

void SelfLocalizationSelector::ownStart()
{
  odometry_estimated_pose_sub = node_handle.subscribe("/" + drone_id_namespace + "/" + odometry_estimated_pose_str, 1,
                                                      &SelfLocalizationSelector::odometryEstimatedPoseCallback, this);
  odometry_estimated_speed_sub = node_handle.subscribe("/" + drone_id_namespace + "/" + odometry_estimated_speed_str, 1,
                                                       &SelfLocalizationSelector::odometryEstimatedSpeedCallback, this);
  aruco_slam_estimated_pose_sub =
      node_handle.subscribe("/" + drone_id_namespace + "/" + aruco_slam_estimated_pose_str, 1,
                            &SelfLocalizationSelector::arucoSlamEstimatedPoseCallback, this);
  aruco_slam_estimated_speed_sub =
      node_handle.subscribe("/" + drone_id_namespace + "/" + aruco_slam_estimated_speed_str, 1,
                            &SelfLocalizationSelector::arucoSlamEstimatedSpeedCallback, this);
  frame_based_estimated_pose_sub =
      node_handle.subscribe("/" + drone_id_namespace + "/" + frame_based_estimated_pose_str, 1,
                            &SelfLocalizationSelector::frameBasedEstimationPoseCallback, this);
  frame_based_estimated_speed_sub =
      node_handle.subscribe("/" + drone_id_namespace + "/" + frame_based_estimated_speed_str, 1,
                            &SelfLocalizationSelector::frameBasedEstimationSpeedCallback, this);
  estimated_pose_pub =
      node_handle.advertise<droneMsgsROS::dronePose>("/" + drone_id_namespace + "/" + estimated_pose_str, 1);
  estimated_speed_pub =
      node_handle.advertise<droneMsgsROS::droneSpeeds>("/" + drone_id_namespace + "/" + estimated_speed_str, 1);
  geometric_pose_pub = node_handle.advertise<geometry_msgs::PoseStamped>(
      "/" + drone_id_namespace + "/" + geometric_pose_str, 1);
  geometric_speed_pub = node_handle.advertise<geometry_msgs::TwistStamped>(
      "/" + drone_id_namespace + "/" + geometric_speed_str, 1);
}

void SelfLocalizationSelector::ownRun()
{
  switch (mode)
  {
    case odometry:
    {
      odometry_new_pose_msg.pose.position.x = odometry_estimated_pose_msg.x;
      odometry_new_pose_msg.pose.position.y = odometry_estimated_pose_msg.y;
      odometry_new_pose_msg.pose.position.z = odometry_estimated_pose_msg.z;

      Eigen::Quaternionf q;
      q = toQuaternion(odometry_estimated_pose_msg.yaw, odometry_estimated_pose_msg.pitch,
                       odometry_estimated_pose_msg.roll);

      odometry_new_pose_msg.pose.orientation.x = q.x();
      odometry_new_pose_msg.pose.orientation.y = q.y();
      odometry_new_pose_msg.pose.orientation.z = q.z();
      odometry_new_pose_msg.pose.orientation.w = q.w();
      geometric_pose_pub.publish(odometry_new_pose_msg);
      estimated_pose_pub.publish(odometry_estimated_pose_msg);

      odometry_new_speed_msg.twist.linear.x = odometry_estimated_speed_msg.dx;
      odometry_new_speed_msg.twist.linear.y = odometry_estimated_speed_msg.dy;
      odometry_new_speed_msg.twist.linear.z = odometry_estimated_speed_msg.dz;
      odometry_new_speed_msg.twist.angular.x = odometry_estimated_speed_msg.dyaw;
      odometry_new_speed_msg.twist.angular.y = odometry_estimated_speed_msg.dpitch;
      odometry_new_speed_msg.twist.angular.z = odometry_estimated_speed_msg.droll;
      geometric_speed_pub.publish(odometry_new_speed_msg);
      estimated_speed_pub.publish(odometry_estimated_speed_msg);
      break;
    }
    case ekf_fusion:
    {
      odometry_new_pose_msg.pose.position.x = odometry_estimated_pose_msg.x;
      odometry_new_pose_msg.pose.position.y = odometry_estimated_pose_msg.y;
      odometry_new_pose_msg.pose.position.z = odometry_estimated_pose_msg.z;

      Eigen::Quaternionf q;
      q = toQuaternion(odometry_estimated_pose_msg.yaw, odometry_estimated_pose_msg.pitch,
                       odometry_estimated_pose_msg.roll);

      odometry_new_pose_msg.pose.orientation.x = q.x();
      odometry_new_pose_msg.pose.orientation.y = q.y();
      odometry_new_pose_msg.pose.orientation.z = q.z();
      odometry_new_pose_msg.pose.orientation.w = q.w();
      geometric_pose_pub.publish(odometry_new_pose_msg);
      estimated_pose_pub.publish(odometry_estimated_pose_msg);

      odometry_new_speed_msg.twist.linear.x = odometry_estimated_speed_msg.dx;
      odometry_new_speed_msg.twist.linear.y = odometry_estimated_speed_msg.dy;
      odometry_new_speed_msg.twist.linear.z = odometry_estimated_speed_msg.dz;
      odometry_new_speed_msg.twist.angular.x = odometry_estimated_speed_msg.dyaw;
      odometry_new_speed_msg.twist.angular.y = odometry_estimated_speed_msg.dpitch;
      odometry_new_speed_msg.twist.angular.z = odometry_estimated_speed_msg.droll;
      geometric_speed_pub.publish(odometry_new_speed_msg);
      estimated_speed_pub.publish(odometry_estimated_speed_msg);
      break;
    }
    case visual_markers:
    {
      aruco_slam_new_pose_msg.pose.position.x = aruco_slam_estimated_pose_msg.x;
      aruco_slam_new_pose_msg.pose.position.y = aruco_slam_estimated_pose_msg.y;
      aruco_slam_new_pose_msg.pose.position.z = aruco_slam_estimated_pose_msg.z;
      Eigen::Quaternionf q;
      q = toQuaternion(aruco_slam_estimated_pose_msg.yaw, aruco_slam_estimated_pose_msg.pitch,
                       aruco_slam_estimated_pose_msg.roll);

      aruco_slam_new_pose_msg.pose.orientation.x = q.x();
      aruco_slam_new_pose_msg.pose.orientation.y = q.y();
      aruco_slam_new_pose_msg.pose.orientation.z = q.z();
      aruco_slam_new_pose_msg.pose.orientation.w = q.w();
      estimated_pose_pub.publish(aruco_slam_estimated_pose_msg);
      geometric_pose_pub.publish(aruco_slam_new_pose_msg);

      aruco_slam_new_speed_msg.twist.linear.x = aruco_slam_estimated_speed_msg.dx;
      aruco_slam_new_speed_msg.twist.linear.y = aruco_slam_estimated_speed_msg.dy;
      aruco_slam_new_speed_msg.twist.linear.z = aruco_slam_estimated_speed_msg.dz;
      aruco_slam_new_speed_msg.twist.angular.x = aruco_slam_estimated_speed_msg.dyaw;
      aruco_slam_new_speed_msg.twist.angular.y = aruco_slam_estimated_speed_msg.dpitch;
      aruco_slam_new_speed_msg.twist.angular.z = aruco_slam_estimated_speed_msg.droll;
      geometric_speed_pub.publish(aruco_slam_new_speed_msg);
      estimated_speed_pub.publish(aruco_slam_estimated_speed_msg);
      break;
    }
    case frame:
    {
      frame_based_new_pose_msg.pose.position.x = frame_based_estimated_pose_msg.x;
      frame_based_new_pose_msg.pose.position.y = frame_based_estimated_pose_msg.y;
      frame_based_new_pose_msg.pose.position.z = frame_based_estimated_pose_msg.z;
      Eigen::Quaternionf q;
      q = toQuaternion(frame_based_estimated_pose_msg.yaw, frame_based_estimated_pose_msg.pitch,
                       frame_based_estimated_pose_msg.roll);
      frame_based_new_pose_msg.pose.orientation.x = q.x();
      frame_based_new_pose_msg.pose.orientation.y = q.y();
      frame_based_new_pose_msg.pose.orientation.z = q.z();
      frame_based_new_pose_msg.pose.orientation.w = q.w();
      estimated_pose_pub.publish(frame_based_estimated_pose_msg);
      geometric_pose_pub.publish(frame_based_new_pose_msg);

      frame_based_new_speed_msg.twist.linear.x = frame_based_estimated_speed_msg.dx;
      frame_based_new_speed_msg.twist.linear.y = frame_based_estimated_speed_msg.dy;
      frame_based_new_speed_msg.twist.linear.z = frame_based_estimated_speed_msg.dz;
      frame_based_new_speed_msg.twist.angular.x = frame_based_estimated_speed_msg.dyaw;
      frame_based_new_speed_msg.twist.angular.y = frame_based_estimated_speed_msg.dpitch;
      frame_based_new_speed_msg.twist.angular.z = frame_based_estimated_speed_msg.droll;
      geometric_speed_pub.publish(frame_based_new_speed_msg);
      estimated_speed_pub.publish(frame_based_estimated_speed_msg);
      break;
    }
  }
}

void SelfLocalizationSelector::ownStop()
{
  odometry_estimated_pose_sub.shutdown();
  odometry_estimated_speed_sub.shutdown();
  aruco_slam_estimated_pose_sub.shutdown();
  aruco_slam_estimated_speed_sub.shutdown();
  frame_based_estimated_pose_sub.shutdown();
  frame_based_estimated_speed_sub.shutdown();
}

Eigen::Quaternionf SelfLocalizationSelector::toQuaternion(double yaw, double pitch,
                                                          double roll)  // yaw (Z), pitch (Y), roll (X)
{
  // Abbreviations for the various angular functions
  double cy = cos(yaw * 0.5);
  double sy = sin(yaw * 0.5);
  double cp = cos(pitch * 0.5);
  double sp = sin(pitch * 0.5);
  double cr = cos(roll * 0.5);
  double sr = sin(roll * 0.5);

  Eigen::Quaternionf q;
  q.w() = cy * cp * cr + sy * sp * sr;
  q.x() = cy * cp * sr - sy * sp * cr;
  q.y() = sy * cp * sr + cy * sp * cr;
  q.z() = sy * cp * cr - cy * sp * sr;
  return q;
}

/*Callbacks*/
bool SelfLocalizationSelector::changeModeToOdometryCallback(std_srvs::EmptyRequest& request,
                                                            std_srvs::EmptyResponse& response)
{
  mode = odometry;
  std::cout << "Change mode to ODOMETRY" << std::endl;
  return true;
}

bool SelfLocalizationSelector::changeModeToEkfSensorFusionCallback(std_srvs::EmptyRequest& request,
                                                            std_srvs::EmptyResponse& response)
{
  mode = ekf_fusion;
  std::cout << "Change mode to EKF fusion" << std::endl;
  return true;
}

bool SelfLocalizationSelector::changeModeToVisualMarkersCallback(std_srvs::EmptyRequest& request,
                                                                 std_srvs::EmptyResponse& response)
{
  mode = visual_markers;
  std::cout << "Change mode to VISUAL_MARKERS" << std::endl;
  return true;
}

bool SelfLocalizationSelector::changeModeToFrameBasedLocalizationCallback(std_srvs::EmptyRequest& request,
                                                                          std_srvs::EmptyResponse& response)
{
  mode = frame;
  std::cout << "Change mode to frame" << std::endl;
  return true;
}

void SelfLocalizationSelector::odometryEstimatedPoseCallback(const droneMsgsROS::dronePose& message)
{
  odometry_estimated_pose_msg = message;
}

void SelfLocalizationSelector::odometryEstimatedSpeedCallback(const droneMsgsROS::droneSpeeds& message)
{
  odometry_estimated_speed_msg = message;
}

void SelfLocalizationSelector::arucoSlamEstimatedPoseCallback(const droneMsgsROS::dronePose& message)
{
  aruco_slam_estimated_pose_msg = message;
}

void SelfLocalizationSelector::arucoSlamEstimatedSpeedCallback(const droneMsgsROS::droneSpeeds& message)
{
  aruco_slam_estimated_speed_msg = message;
}

void SelfLocalizationSelector::frameBasedEstimationPoseCallback(const droneMsgsROS::dronePose& message)
{
  frame_based_estimated_pose_msg = message;
}

void SelfLocalizationSelector::frameBasedEstimationSpeedCallback(const droneMsgsROS::droneSpeeds& message)
{
  frame_based_estimated_speed_msg = message;
}
