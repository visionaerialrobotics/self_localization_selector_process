/*!*********************************************************************************
 *  \file       self_localization_process.h
 *  \brief      SelfLocalizationSelector definition file.
 *  \details    This file contains the SelfLocalizationSelector declaration. To obtain more information about
 *              it's definition consult the self_localization_process.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef SELF_LOCALIZATION_SELECTOR_PROCESS_H
#define SELF_LOCALIZATION_SELECTOR_PROCESS_H

/*System*/
#include <string>
#include <ros/ros.h>
#include <math.h>
/*Messages*/
#include <std_srvs/Empty.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneSpeeds.h>
/*Aerostack*/
#include <drone_process.h>
#include <tf/tf.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <Eigen/Geometry>
#include <angles/angles.h>

/*Mode definition*/
enum Mode
{
  odometry,
  visual_markers,
  ekf_fusion,
  frame
};

/*Class definition*/
class SelfLocalizationSelector : public DroneProcess
{
public:
  SelfLocalizationSelector();
  ~SelfLocalizationSelector();

private:
  ros::NodeHandle node_handle;
  Mode mode;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;
  std::string change_mode_to_odometry_str;
  std::string change_mode_to_ekf_sensor_fusion_str;
  std::string change_mode_to_visual_markers_str;
  std::string change_mode_to_frame_based_localization_str;
  std::string odometry_estimated_pose_str;
  std::string odometry_estimated_speed_str;
  std::string aruco_slam_estimated_pose_str;
  std::string aruco_slam_estimated_speed_str;
  std::string estimated_pose_str;
  std::string estimated_speed_str;
  std::string geometric_pose_str;
  std::string geometric_speed_str;
  std::string frame_based_estimated_pose_str;
  std::string frame_based_estimated_speed_str;
  std::string self_localization_ns_str;

  geometry_msgs::PoseStamped odometry_new_pose_msg;
  geometry_msgs::TwistStamped odometry_new_speed_msg;
  geometry_msgs::TwistStamped aruco_slam_new_speed_msg;
  geometry_msgs::PoseStamped aruco_slam_new_pose_msg;
  geometry_msgs::TwistStamped frame_based_new_speed_msg;
  geometry_msgs::PoseStamped frame_based_new_pose_msg;

  ros::ServiceServer change_mode_to_odometry_srv;
  ros::ServiceServer change_mode_to_ekf_sensor_fusion_srv;
  ros::ServiceServer change_mode_to_visual_markers_srv;
  ros::ServiceServer change_mode_to_frame_based_localization_srv;
  ros::Subscriber odometry_estimated_pose_sub;
  ros::Subscriber odometry_estimated_speed_sub;
  ros::Subscriber aruco_slam_estimated_pose_sub;
  ros::Subscriber aruco_slam_estimated_speed_sub;
  ros::Subscriber frame_based_estimated_pose_sub;
  ros::Subscriber frame_based_estimated_speed_sub;
  ros::Publisher estimated_pose_pub;
  ros::Publisher estimated_speed_pub;
  ros::Publisher geometric_pose_pub;
  ros::Publisher geometric_speed_pub;

  droneMsgsROS::dronePose odometry_estimated_pose_msg;
  droneMsgsROS::droneSpeeds odometry_estimated_speed_msg;
  droneMsgsROS::dronePose aruco_slam_estimated_pose_msg;
  droneMsgsROS::droneSpeeds aruco_slam_estimated_speed_msg;
  droneMsgsROS::dronePose frame_based_estimated_pose_msg;
  droneMsgsROS::droneSpeeds frame_based_estimated_speed_msg;

private:
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
  Eigen::Quaternionf toQuaternion(double yaw, double pitch, double roll);

  /*Callbacks*/
private:
  bool changeModeToOdometryCallback(std_srvs::EmptyRequest&, std_srvs::EmptyResponse&);
  bool changeModeToEkfSensorFusionCallback(std_srvs::EmptyRequest&, std_srvs::EmptyResponse&);
  bool changeModeToVisualMarkersCallback(std_srvs::EmptyRequest&, std_srvs::EmptyResponse&);
  bool changeModeToFrameBasedLocalizationCallback(std_srvs::EmptyRequest&, std_srvs::EmptyResponse&);
  void odometryEstimatedPoseCallback(const droneMsgsROS::dronePose&);
  void odometryEstimatedSpeedCallback(const droneMsgsROS::droneSpeeds&);
  void arucoSlamEstimatedPoseCallback(const droneMsgsROS::dronePose&);
  void arucoSlamEstimatedSpeedCallback(const droneMsgsROS::droneSpeeds&);
  void frameBasedEstimationPoseCallback(const droneMsgsROS::dronePose&);
  void frameBasedEstimationSpeedCallback(const droneMsgsROS::droneSpeeds&);
};

#endif
