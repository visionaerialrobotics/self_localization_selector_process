# Brief
This process publishes the pose and the speed of the drone depending on the mode selected.
The are three different modes, the odometry mode, the ekf fusion mode and the aruco slam mode.

# Services
- **change_self_localization_mode_to_odometry** ([std_srvs/Empty](http://docs.ros.org/kinetic/api/std_srvs/html/srv/Empty.html))  
Changes the mode of the selector to odometry.

- **change_self_localization_mode_to_visual_markers** ([std_srvs/Empty](http://docs.ros.org/kinetic/api/std_srvs/html/srv/Empty.html))  
Changes the mode of the selector to visual markers.

- **change_self_localization_mode_to_ekf_sensor_fusion** ([std_srvs/Empty](http://docs.ros.org/kinetic/api/std_srvs/html/srv/Empty.html))  
Changes the mode of the selector to ekf sensor fusion.


# Published topics

- **self_localization/pose** ([geometry_msgs::PoseStamped](http://docs.ros.org/api/geometry_msgs/html/msg/PoseStamped.html))  
Publishes the pose (in quaternions) of the drone based on the mode of the selector.

- **self_localization/speed** ([geometry_msgs::Twist](http://docs.ros.org/api/geometry_msgs/html/msg/Twist.html))  
Publishes the speed of the drone based on the mode of the selector.

# Subscribed topics
- **ArucoSlam_EstimatedPose** ([droneMsgsROS/dronePose](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/msg/dronePose.msg))  
Contains the estimated pose of the drone based on visual markers

- **ArucoSlam_EstimatedSpeed** ([droneMsgsROS/droneSpeeds](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/msg/droneSpeeds.msg))  
Contains the estimated speed of the drone based on visual markers

- **EstimatedPose_droneGMR_wrt_GFF** ([droneMsgsROS/dronePose](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/msg/dronePose.msg))  
Contains the estimated pose of the drone based on odometry.

- **EstimatedSpeed_droneGMR_wrt_GFF** ([droneMsgsROS/droneSpeeds](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/msg/droneSpeeds.msg))  
Contains the estimated speed of the drone based on odometry.

# Quaternion transformation
The formula used to transform the data received in roll, pitch, yaw (RPY) to quaternion representation is described below:  

```c++
roll_rad      =  (msg.vector.x) * (M_PI/180);
pitch_rad     = -(msg.vector.y) * (M_PI/180);
yaw_rad       = -(msg.vector.z) * (M_PI/180);

tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll_rad, pitch_rad, yaw_rad);

rotationAngleData.orientation.x = quaternion.getX();
rotationAngleData.orientation.y = quaternion.getY();
rotationAngleData.orientation.z = quaternion.getZ();
rotationAngleData.orientation.w = quaternion.getW();
```


---
# Contributors
**Maintainer:** Alberto Camporredondo (alberto.camporredondo@gmail.com)  
**Author:** Alberto Camporredondo

